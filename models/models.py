# -*- coding: utf-8 -*-

from odoo import models, fields, api

class interneval(models.Model):
    _name = 'jobapp.interneval'
    _description = 'Contains extra details for the Mass Mailing Module'



 #----applicant's details-----------------------------------
    applicant = fields.Text(string='Name:', required=True)
    gender = fields.Selection([
        ('0','Male'),
        ('1','Female')], string='Gender:', required=True)
    email = fields.Char(string='Email:', size=52, required=True)
    phone_no = fields.Char(string='Phone Number:', required=True)
    position = fields.Char(string='Job Position:', size=52, required=True)
    address = fields.Char(string='Postal Address:', size=52, required=True)
    online_link = fields.Char(string='Github or Bitbucket Link:', size=52, required=True)

#-----for the attachment, it's a trial---
#----for the attachment, it works...yaay!---
    #for the CV
    upload_cv = fields.Binary(string="Upload your CV here:", required=True)
    #file_name = fields.Char(string="File Name")

    #for the cover letter
    upload_letter =fields.Binary(string="Upload your Cover Letter here:", required=True)
    #file_upload=fields.Char(string="File Upload")

#----this is a test for the print function---
    @api.multi
    def print_report(self):
        return self.env['report'].get_action(self, 'interneval.report_job')
